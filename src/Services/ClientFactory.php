<?php

/**
 * @file
 * Contains \Drupal\europeana\Services\ClientFactory.
 */

namespace Drupal\europeana\Services;

use Colada\Europeana\Transport\ApiClient;
use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\ClientInterface;


class ClientFactory
{
  protected $http_client;

  protected $api_key;

  /**
   * Constructor
   *
   * @param ClientInterface        $http_client    [description]
   * @param ConfigFactoryInterface $config_factory [description]
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config_factory)
  {
    $config = $config_factory->get('europeana.settings');
    $this->api_key = $config->get('api_key');
    $this->http_client = $http_client;
  }

  /**
   * [get description]
   * @return [type] [description]
   */
  public function get()
  {
    return new ApiClient($this->api_key, $this->http_client);
  }
}
