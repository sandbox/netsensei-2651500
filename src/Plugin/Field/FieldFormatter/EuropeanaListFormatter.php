<?php

/**
 * @file
 * Contains \Drupal\europeana\Plugin\Field\FieldFormatter\EuropeanaListFormatter.
 */

namespace Drupal\europeana\Plugin\Field\FieldFormatter;

use Colada\Europeana\Transport\ApiClientInterface;
use Colada\Europeana\Payload\SearchPayload;
use Colada\Europeana\Payload\Facet\Refinement;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'europeana_list' formatter.
 *
 * @FieldFormatter(
 *   id = "europeana_list",
 *   label = @Translation("Europeana list"),
 *   field_types = {
 *     "europeana_search"
 *   }
 * )
 */
class EuropeanaListFormatter extends FormatterBase implements ContainerFactoryPluginInterface {


  protected $apiClient;

  /**
   * Constructs a FormatterBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, ApiClientInterface $apiClient) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->apiClient = $apiClient;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('europeana.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'title' => '',
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title to replace basic numeric telephone number display'),
      '#default_value' => $this->getSetting('title'),
    );

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = array();
    $settings = $this->getSettings();

    if (!empty($settings['title'])) {
      $summary[] = t('Link using text: @title', array('@title' => $settings['title']));
    }
    else {
      $summary[] = t('Link using provided telephone number.');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = array();
    $title_setting = $this->getSetting('title');

    foreach ($items as $delta => $item) {
      try {
        $searchPayload = new SearchPayload();

        $searchPayload->setQuery($item->value);
        $searchPayload->setStart(1);
        $searchPayload->setRows($item->rows);
        $searchPayload->setReusability($item->reusability);

        if (isset($item->type)) {
          $typeRefinement = new Refinement('TYPE', $item->type);
          $searchPayload->addRefinement($typeRefinement);
        }

        $response = $this->apiClient->send($searchPayload);
        if (!empty($response->getItems())) {
          $items = $response->getItems()->map(function ($item) {
            return array(
              '#theme' => 'search_item',
              '#item' => $item,
            );
          });

          $element[$delta] = array(
            '#theme' => 'item_list',
            '#wrapper_attributes' => ['class' => 'europeana'],
            '#items' => $items->toArray(),
          );
        }
      } catch (\Exception $e) {
        \Drupal::logger('europeana')->error($e->getMessage());
        drupal_set_message($e->getMessage(), 'error');
      }
    }

    return $element;
  }
}
