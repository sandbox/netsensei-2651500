<?php

/**
 * @file
 * Contains \Drupal\europeana\Plugin\Field\FieldWidget\EuropeanaSearchWidget.
 */

namespace Drupal\europeana\Plugin\Field\FieldWidget;

use Colada\Europeana\Enum\Reusability;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'europeana_search' widget.
 *
 * @FieldWidget(
 *   id = "europeana_search",
 *   label = @Translation("Europeana Search"),
 *   field_types = {
 *     "europeana_search"
 *   }
 * )
 */
class EuropeanaSearchWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'placeholder' => '',
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['placeholder'] = array(
      '#type' => 'textfield',
      '#title' => t('Placeholder'),
      '#default_value' => $this->getSetting('placeholder'),
      '#description' => t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    );
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = array();

    $placeholder = $this->getSetting('placeholder');
    if (!empty($placeholder)) {
      $summary[] = t('Placeholder: @placeholder', array('@placeholder' => $placeholder));
    }
    else {
      $summary[] = t('No placeholder');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $item = $items[$delta];

    $element['value'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Search query'),
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
      '#maxlength' => 2048,
      '#required' => $element['#required'],
    );

    $element['rows'] = array(
      '#type' => 'select',
      '#title' => $this->t('Rows'),
      '#options' => range(1, 100),
      '#default_value' => isset($items[$delta]->rows) ? $items[$delta]->rows : NULL,
    );

    $options = Reusability::getAll();

    $element['reusability'] = array(
      '#type' => 'select',
      '#title' => $this->t('Reusability'),
      '#options' => array_combine($options, $options),
      '#default_value' => isset($items[$delta]->reusability) ? $items[$delta]->reusability : NULL,
    );

    $options = array(
      'TEXT' => t('Text'),
      'VIDEO' => t('Video'),
      'SOUND' => t('Sound'),
      'IMAGE' => t('Image'),
      '3D' => t('3D'),
    );
    $element['type'] = array(
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#options' => $options,
      '#empty_option' => t('- None -'),
      '#default_value' => isset($items[$delta]->type) ? $items[$delta]->type : NULL,
    );

    // If cardinality is 1, ensure a proper label is output for the field.
    if ($this->fieldDefinition->getFieldStorageDefinition()->getCardinality() == 1) {
      $element += array(
       '#type' => 'fieldset',
      );
    }


    return $element;
  }

}
