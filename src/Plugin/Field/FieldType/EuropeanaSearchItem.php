<?php

/**
 * @file
 * Contains \Drupal\europeana\Plugin\Field\FieldType\EuropeanaSearchItem.
 */

namespace Drupal\europeana\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'europeana_search' field type.
 *
 * @FieldType(
 *   id = "europeana_search",
 *   label = @Translation("Europeana search"),
 *   description = @Translation("This field stores an Europeana search in the database."),
 *   category = @Translation("Europeana"),
 *   default_widget = "europeana_search",
 *   default_formatter = "europeana_list"
 * )
 */
class EuropeanaSearchItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'value' => array(
          'type' => 'text',
          'length' => 2048,
        ),
        'rows' => array(
          'type' => 'int',
          'size' => 'tiny',
        ),
        'reusability' => array(
          'type' => 'varchar',
          'length' => 256,
        ),
        'type' => array(
          'type' => 'varchar',
          'length' => 256,
        )
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Search query'))
      ->setRequired(TRUE);

    $properties['rows'] = DataDefinition::create('integer')
      ->setLabel(t('Rows'))
      ->setRequired(FALSE);

    $properties['reusability'] = DataDefinition::create('string')
      ->setLabel(t('Reusability'))
      ->setRequired(FALSE);

    $properties['type'] = DataDefinition::create('string')
      ->setLabel(t('Type'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();
    $constraints = parent::getConstraints();

    $max_length = 2048;
    $constraints[] = $constraint_manager->create('ComplexData', array(
      'value' => array(
        'Length' => array(
          'max' => $max_length,
          'maxMessage' => t('%name: the search query may not be longer than @max characters.', array('%name' => $this->getFieldDefinition()->getLabel(), '@max' => $max_length)),
        )
      ),
    ));

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $values['value'] = rand(pow(10, 8), pow(10, 9)-1);
    return $values;
  }

}
