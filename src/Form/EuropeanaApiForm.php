<?php

/**
 * @file
 * Contains \Drupal\system\europeana\EuropeanaApiForm.
 */

namespace Drupal\europeana\Form;

use Colada\Europeana\Transport\ApiClientInterface;
use Colada\Europeana\Payload\ProvidersPayload;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Europeana settings for this site.
 */
class EuropeanaApiForm extends ConfigFormBase {

  protected $apiClient;

  public function __construct(ApiClientInterface $apiClient) {
    $this->apiClient = $apiClient;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('europeana.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'europeana_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['europeana.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('europeana.settings');

    // @todo
    //   Should only be run on validation. Then stored in state API. (?)
    try {
      $payload = new ProvidersPayload();
      $payload->setOffset(1);
      $payload->setPageSize(1);
      $payloadResponse = $this->apiClient->send($payload);
      drupal_set_message('API key verified. Drupal can successfully connect with the Europeana API.');
    } catch (\Exception $e) {
      drupal_set_message($e->getMessage(), 'error');
    }

    $form['api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Europeana API key'),
      '#default_value' => $config->get('api_key'),
      '#description' => t('Your Europeana API key.'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('europeana.settings')
      ->set('api_key', $form_state->getValue('api_key'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
