<?php

namespace Drupal\europeana\Controller;

use Colada\Europeana\Transport\ApiClientInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ApiController extends ControllerBase {

  protected $apiClient;

  public function __construct(ApiClientInterface $apiClient) {
    $this->apiClient = $apiClient;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('europeana.client')
    );
  }

  public function page() {
    return array(
      '#markup' => t('foo bar')
    );
  }
}
